package ru.kondratenko.jse56.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@MappedSuperclass
public class MyEntity<E> {
    @Column(nullable = false, length = 100)
    private String name;

    public MyEntity(String name) {
        this.name = name;
    }
}
