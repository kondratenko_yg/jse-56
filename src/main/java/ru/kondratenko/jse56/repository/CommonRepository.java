package ru.kondratenko.jse56.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.kondratenko.jse56.config.HibernateConfig;
import ru.kondratenko.jse56.model.Chat;
import ru.kondratenko.jse56.model.Message;
import ru.kondratenko.jse56.model.User;

import javax.persistence.NoResultException;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public class CommonRepository {

    private SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> void save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(entity);

        tx.commit();
        session.close();
    }

    public <T> T getById(Class<T> entityClass, Serializable id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        T entity = session.get(entityClass, id);

        tx.commit();
        session.close();

        return entity;
    }

    public <T> Optional<T> getByName(Class<T> entityClass, String name) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Optional<T> entity = Optional.empty();

        try {
            entity = Optional.ofNullable(session.createQuery("SELECT u FROM " + entityClass.getName() + " u where u.name = :name", entityClass)
                    .setParameter("name", name).getSingleResult());
        } catch (NoResultException e) {
        }

        tx.commit();
        session.close();

        return entity;
    }

    public List<Message> getMessages(Chat chat, User currentUser) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Instant time = Instant.now();
        Instant time1 = time.minusSeconds(1);

        List<Message> messages = session.createQuery("SELECT m from Message m " +
                "inner join m.user as c_user " +
                "inner join m.chat as chat " +
                "where c_user<>:currentUser and " +
                "chat=:chat and " +
                "m.time between :time1 and :time0", Message.class)
                .setParameter("currentUser", currentUser)
                .setParameter("chat", chat)
                .setParameter("time1", time1)
                .setParameter("time0", time)
                .getResultList();

        tx.commit();
        session.close();

        return messages;
    }

    // JPQL
    public List<User> getAllUsers() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        List<User> cars = session.createQuery("SELECT c from User c", User.class).getResultList();

        tx.commit();
        session.close();

        return cars;
    }
}
